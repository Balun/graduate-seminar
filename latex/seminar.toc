\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Povijesni pregled podr\IeC {\v z}anog u\IeC {\v c}enja}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}op\IeC {\'c}enito}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Dinami\IeC {\v c}ko programiranje}{2}{section.2.2}
\contentsline {section}{\numberline {2.3}Teorija optimalnog upravljanja}{3}{section.2.3}
\contentsline {section}{\numberline {2.4}Markovljevi procesi odu\IeC {\v c}ivanja}{3}{section.2.4}
\contentsline {section}{\numberline {2.5}Metode poku\IeC {\v s}aja i pogre\IeC {\v s}ke}{4}{section.2.5}
\contentsline {chapter}{\numberline {3}Formulacija i elementi podr\IeC {\v z}anog u\IeC {\v c}enja}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Politika}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Nagrada}{6}{section.3.2}
\contentsline {section}{\numberline {3.3}Funkcija vrijednosti}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}Model okoline}{8}{section.3.4}
\contentsline {chapter}{\numberline {4}Q-u\IeC {\v c}enje: konkretni primjer podr\IeC {\v z}anog u\IeC {\v c}enja}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Formalne definicije}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Proces odlu\IeC {\v c}ivanja}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Q-u\IeC {\v c}enje}{10}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Primjena Q-u\IeC {\v c}enja}{12}{section.4.2}
\contentsline {chapter}{\numberline {5}Zaklju\IeC {\v c}ak}{14}{chapter.5}
\contentsline {chapter}{\numberline {6}Literatura}{15}{chapter.6}
\contentsline {chapter}{\numberline {7}Sa\IeC {\v z}etak}{16}{chapter.7}
